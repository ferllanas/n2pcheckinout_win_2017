/*
 *  XMLtoAssembler.h
 *  N2PCheckInOut
 *
 *  Created by Juan Fernando Llanas Rodriguez on 12/12/14.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __InterlasaUtilities_H_DEFINED__
#define __InterlasaUtilities_H_DEFINED__

/*#include "IParcel.h"
#include "ITextParcelList.h"
#include <IHierarchy.h>//aÒadio maribel
*/

//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")

class IDocument;
class IControlView;
class IWindow;


/** A collection of miscellaneous utility methods used by this plug-in.
 @ingroup paneltreeview
 */
class XMLtoAssembler
{
	
public:
	static ErrorCode importXMLToAssembler(const UIDRef & 	documentUIDRef,
								const IDFile & 	targetFile,
								const XMLReference & 	xmlReferenceWhere	);
	
};

#endif // __PnlTrvUtils_H_DEFINED__
