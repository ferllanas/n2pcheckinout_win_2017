//========================================================================================
//  
//  $File: //depot/devtech/nevis/plugin/source/sdksamples/wlistboxcomposite/WLBCmpTreeViewAdapter.cpp $
//  
//  Owner: Danielle Darling
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2017/03/10 00:54:55 $
//  
//  $Revision: #8 $
//  
//  $Change: 979292 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "IStringListData.h"
#include "ListTreeViewAdapter.h"
#include "N2PCheckInOut_NodeID.h"
#include "N2PCheckInOutID.h"
#include "CAlert.h"

/**
 *  TreeViewAdapter.
	@ingroup wlistboxcomposite
 */
class WLBCmpTreeViewAdapter : public ListTreeViewAdapter
{
public:
	WLBCmpTreeViewAdapter(IPMUnknown* boss);
	
	virtual ~WLBCmpTreeViewAdapter()
	{}
	
	virtual int32	GetNumListItems() const;
	NodeID_rv GetRootNode() const;
	virtual NodeID_rv	GetNthListItem(const int32& nth) const;

};

CREATE_PMINTERFACE(WLBCmpTreeViewAdapter, kWLBCmpTVHierarchyAdapterImpl)

WLBCmpTreeViewAdapter::WLBCmpTreeViewAdapter(IPMUnknown* boss):ListTreeViewAdapter(boss)
{
    //CAlert::InformationAlert("WLBCmpTreeViewAdapter 01");
	//initialize the list with the default list string name
/*  
    K2Vector<PMString> lists;
	for (int32 i = 0; i< 12; i++)
	{
		PMString name("Fernando");
		name.AppendNumber(i+1);
		name.Translate();
		lists.push_back(name);
	}
	InterfacePtr<IStringListData> iListData(this, IID_ISTRINGLISTDATA);
	iListData->SetStringList(lists);
*/

}
int32 WLBCmpTreeViewAdapter::GetNumListItems()const
{
    // CAlert::InformationAlert("GetNumListItems 01");
	InterfacePtr<IStringListData> iListData(this, IID_ISTRINGLISTDATA);

	return iListData->GetStringList().size();
}

NodeID_rv WLBCmpTreeViewAdapter::GetRootNode() const
{
    //CAlert::InformationAlert("GetRootNode 01");
	PMString rootString("Root");
	rootString.Translate();
	return PageCmpNodeID::Create(rootString);
}

NodeID_rv WLBCmpTreeViewAdapter::GetNthListItem(const int32& nth) const
{
    //CAlert::InformationAlert("GetNthListItem 01");
	InterfacePtr<IStringListData> iListData(this, IID_ISTRINGLISTDATA);

	return PageCmpNodeID::Create(iListData->GetStringList()[nth]);	
}
