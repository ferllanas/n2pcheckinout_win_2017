//========================================================================================
//  
//  $File: //depot/devtech/nevis/plugin/source/sdksamples/wlistboxcomposite/WLBCmpRezDefs.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2017/03/10 00:54:55 $
//  
//  $Revision: #8 $
//  
//  $Change: 979292 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Defines Resource IDs used by the WLBCmp plug-in.
//  
//========================================================================================

#ifndef __WLBCmpRezDefs_h__
#define __WLBCmpRezDefs_h__

#define kWLBCmpListElementRsrcID 	1200

// See. the platform resource file where these are used!
#define kWLBCmpEyeBallIcon			1510
#define kWLBCmpPenIcon				1514
#endif // __WLBCmpRezDefs_h__

// End, WLBCmpRezDefs.h.


