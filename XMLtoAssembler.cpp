/*
 *  XMLtoAssembler.cpp
 *  N2PCheckInOut
 *
 *  Created by Juan Fernando Llanas Rodriguez on 12/12/14.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *	No se utilizara por lo pronto problemas al dar checkin done varios usuario al mismo tiempo.
 */
#include "VCPluginHeaders.h"

#include "XMLReference.h"
#include "IImportXMLData.h"
#include "IXMLImportOptions.h"
#include "IPMUnknownData.h"

#include "IDocument.h"
#include "ICommand.h"

#include "XMLtoAssembler.h"

ErrorCode XMLtoAssembler::importXMLToAssembler(const UIDRef & 	documentUIDRef,
											  const IDFile & 	targetFile,
											  const XMLReference & 	xmlReferenceWhere)
{
	ErrorCode err = kFailure;
	do {
		// +precondition
		InterfacePtr<IDocument> document(documentUIDRef, UseDefaultIID());
		ASSERT(document);
		if(!document) {
			break;
		}
		// -precondition
		
		InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportXMLFileCmdBoss));
		ASSERT(importCmd);
		InterfacePtr<IImportXMLData> importXMLData(CreateObject2<IImportXMLData>(kImportXMLDataBoss));
		ASSERT(importXMLData);
		if(!importXMLData) {
			break;
		}
		importXMLData->Set(documentUIDRef.GetDataBase(), targetFile, xmlReferenceWhere, kSuppressUI);
		InterfacePtr<IXMLImportOptions> docXMLOptions( document->GetDocWorkSpace(), UseDefaultIID() );
		ASSERT(docXMLOptions);
		if(!docXMLOptions) {
			break;
		}
		InterfacePtr<IXMLImportOptions> importXMLOptions(importXMLData, UseDefaultIID());
		ASSERT(importXMLOptions);
		if(!importXMLOptions) {
			break;
		}
		importXMLOptions->Copy(docXMLOptions);
		
		InterfacePtr<IPMUnknownData> pmUnknownData(importCmd, UseDefaultIID());
		ASSERT(pmUnknownData);
		if(!pmUnknownData) {
			break;
		}
		pmUnknownData->SetPMUnknown(importXMLData);
	
		err = CmdUtils::ProcessCommand(importCmd);
		ASSERT(err==kSuccess);
	} while(kFalse);
	return err;
}