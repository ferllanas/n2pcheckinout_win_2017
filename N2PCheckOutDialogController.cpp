/*
//	File:	CheckOutDialogController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:

#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "ImportExportUIID.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IOpenManager.h"
#include "ITriStateData.h"
#include "IInCopyDocUtils.h"
#include "IInCopyCoreOverflowHandler.h"
#include "IContainerClassification.h"
#include "ITextFrameColumn.h"

#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "IInCopyStoryList.h"
#include "PreferenceUtils.h"
#include "IInCopyDocUtils.h"
#include "IInCopyViewPrefs.h"
#include "IWorkspace.h"
#include "IDocumentUIUtils.h"
#include "IDocumentPresentation.h"
#include "IActiveContext.h"
#include "IGalleyUtils.h"

#include "Utils.h"
#include "FileUtils.h"


#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"

#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "SDKUtilities.h"
#include "SDKLayoutHelper.h"

#include "PlatformFileSystemIterator.h"

//#include <direct.h>
#include <stdlib.h>
#include <stdio.h>

#include "ITreeViewController.h"
#include "N2PCheckInOut_NodeID.h"
#include "K2Vector.tpp" // For NodeIDList to compile

// Project includes
#include "N2PCheckInOutID.h"
#include "ICheckInOutSuite.h"
//#include "N2PCheckInOutListBoxHelper.h"
#include "N2PNotesStorage.h"

	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"


//#include "A2PPrefID.h"
//#include "N2PsqlID.h"


////////////////


//#include <winreg.h>    //If you are using MS Visual C++ you do not need this
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")
/** CheckOutDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class CheckOutDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckOutDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CheckOutDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
		
	K2Vector<PMString> userandGroups;
	private:


		


		PMString  Aplicar_Usuario_Actual();

		PMString BuscarEnUsuarios(PMString &Busqueda,PMString Campo);

		void Limpiar_Lista_Paginas(PMString ID_Usuario);

		bool16 CreateAndProcessOpenDocCmd(const IDFile& targetFile, UIDRef& newdocUIDRef);


		

		void Llenar_ComboUsuarios();
		
		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText, PMString idusedloged);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		bool16 LLenar_Combo_EstatusNota(PMString EstatusNota);
		
		bool16 ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion);
	
		int32 getIndexSelectedFromComboWidgetID(WidgetID comboBoxWidgetID);

		
	
		ClassDialogCheckOutPaginaSets PreferencesPara;

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckOutDialogController, kCheckOutDialogControllerImpl)

/* ApplyDialogFields
*/
void CheckOutDialogController::InitializeDialogFields(IActiveContext* context) 
{
	////Inicializa Fields CheckIn
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kPaginaWidgetID,"");

	PMString Usuario=this->Aplicar_Usuario_Actual();

	this->Limpiar_Lista_Paginas(Usuario);


	this->Llenar_ComboUsuarios();
	
	
	
	
		

	ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
	(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
	if(checkIn!=nil)
	{
			
		checkIn->ObtenerDatosDeCheckOutDPaginas(PreferencesPara);
	}
	
		Usuario.SetTranslatable(kFalse);
		this->SeleccionarCadenaEnComboBox(Usuario, kComboBoxUsuarioWidgetID);


			
		//Selecciona el Estado Anterior		
		this->LLenar_Combo_EstatusNota(PreferencesPara.Status_PageOut);
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Estado_To_SavePage, kComboBoxStatusWidgetID);
	
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{
				PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
				//Selecciona la seccion Anterior
				this->LLenar_Combo_Publicacion(PreferencesPara.Issue_PageOut, ID_Usuario);
				//this->SeleccionarCadenaEnComboBox(PreferencesPara.Issue_To_SavePage, kComboBoxIssueWidgetID);
		

				//Selecciona la seccion Anterior
		
				this->LLenar_Combo_Seccion(PreferencesPara.Seccion_PageOut);	
				//this->SeleccionarCadenaEnComboBox(PreferencesPara.Seccion_To_SavePage, kComboBoxSeccionWidgetID);
		
		

			PMString Date = InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
	
			PreferencesPara.Date_PageOut.SetTranslatable(kFalse);
			if(PreferencesPara.Date_PageOut.NumUTF16TextChars()<=0)
				SetTextControlData(kDatePubliWidgetID , Date);
			else
	
				if(PreferencesPara.Date_PageOut=="(null)")
					SetTextControlData(kDatePubliWidgetID , Date);
				else
					SetTextControlData(kDatePubliWidgetID , PreferencesPara.Date_PageOut);
	

			if(PreferencesPara.Pagina_PageOut=="(null)")
				PreferencesPara.Pagina_PageOut.SetTranslatable(kFalse);
			else
				SetTextControlData(kPaginaWidgetID , PreferencesPara.Pagina_PageOut);
			// Put code to initialize widget values here.
		}

		

}

/* ValidateDialogFields
*/
WidgetID CheckOutDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;
	// Put code to validate widget values here.
	do
	{
		PMString EditarPriv="";
		PMString RetirarPriv="";
		PMString Id_PaginaSel="";
		PMString N2PSeccion="";
		PMString Pag_Dirigido_A="";
		PMString Consulta="";
		PMString Usuario="";
		
		//Obtiene el nombre de la aplicacion
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				if(app==nil)
				{
					break;
				}
		
				PMString Aplicacion=app->GetApplicationName();
				Aplicacion.SetTranslatable(kFalse);

		//Obtiene el nombre del usuario logeado actualmenete
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					ASSERT_FAIL("Invalid workspace prefs in CheckInOutDialogController::ApplyDialogFields()");
					break;
				}
		
				Usuario=wrkSpcPrefs->GetIDUserLogedString();
				Usuario.SetTranslatable(kFalse);
		
				if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, N2PSeccion)==kFalse)
				{
					ASSERT_FAIL("Error tiene elelemntos en la lista seleccionados");
					result=kPaginasListBoxWidgetID;
					break;
				}
		
		
		//Obtiene Preferencias para conexion 
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
				
				if(UpdateStorys==nil)
				{
					ASSERT_FAIL("Error no puede hacer conexion");
					result=kPaginasListBoxWidgetID;
					break;
				}
		
				PreferencesConnection PrefConections;
				if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					ASSERT_FAIL("Error no encontro preferencias de conexion");
					result=kPaginasListBoxWidgetID;
					break;
				}
		
			//Arma Cadena de coneccion DSN
				
		
				
		
		
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
				(
					kN2PSQLUtilsBoss,	// Object boss/class
					IN2PSQLUtils::kDefaultIID
				)));
			
			
			//Arma Cadena Query para Basee de datos
				Consulta="SELECT Dirigido_a, tipodirigidoa_id FROM pagina WHERE ID=";
				Consulta.Append(Id_PaginaSel);
				
			
			
				K2Vector<PMString> QueryVectorUsario;
			
				if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
					CAlert::InformationAlert("Error Lectura de PReferencias E0012");
				//Realiza Query
		
				Consulta = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Consulta);
				SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Consulta,QueryVectorUsario);
			
                PMString tipodirigidoa_id="";
				//Obtiene el usuario a quien fue dirigido la ultima vez esta Pagina
				if(QueryVectorUsario.Length()>0)
				{
					Pag_Dirigido_A=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVectorUsario[QueryVectorUsario.Length()-1]);
                    tipodirigidoa_id = SQLInterface->ReturnItemContentbyNameColumn("tipodirigidoa_id",QueryVectorUsario[QueryVectorUsario.Length()-1]);
				}
				else
				{
					result=kPaginasListBoxWidgetID;
					break;
				}
			
			
				
			
				//Para verificar si puede hacer check out de la pgina
				if(!tipodirigidoa_id.Contains("1"))
                {
                    if(Pag_Dirigido_A.NumUTF16TextChars()<1)
                    {
                        result=kPaginasListBoxWidgetID;
                        //break;
                    }
                    
                    if(Usuario==Pag_Dirigido_A)
                    {
                        break;
                    }
                    else
                    {
                        if(Aplicacion.Contains("InDesign"))
                        {
                            
                            Consulta="SELECT * FROM Usuarioxgrupo WHERE Id_Usuario='"+Usuario+"' AND Id_Grupo='"+Pag_Dirigido_A+"' AND estatus=0";
                
                            K2Vector<PMString> QueryVectorPrivilegios;
                            if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
                                CAlert::InformationAlert("Error Lectura de PReferencias E0012");
                            SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Consulta,QueryVectorPrivilegios);
			
                            if(QueryVectorPrivilegios.Length()>0)
                            {

                                EditarPriv=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVectorPrivilegios[QueryVectorPrivilegios.Length()-1]);
                                RetirarPriv=SQLInterface->ReturnItemContentbyNameColumn("Id_Grupo",QueryVectorPrivilegios[QueryVectorPrivilegios.Length()-1]);
                            }
                            else
                            {
                                PMString msgMensaje(kN2PCheckInOutNoPrivilegiosPGrupoDUsuarioStringkey);
                                msgMensaje.Translate();
                                msgMensaje.SetTranslatable(kTrue);
						
                                CAlert::InformationAlert(msgMensaje);
                                result=kPaginasListBoxWidgetID;
                                break;
                            }
                        }
                    }
                }
	}while(false);
	return result;
}

/* ApplyDialogFields
*/
void CheckOutDialogController::ApplyDialogFields(IActiveContext* context, const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.
	//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields ini");
	do
	{
		PMString Date="";
		PMString Issue="";
		PMString DirigidoA="";
		PMString Seccion="";
		PMString Id_PaginaSel="";
		
		PMString HoraIn="";
		PMString FechaIn="";
		PMString fecha="";
		PMString Aplicacion="";
		
		//Obtiene el nombre de la aplicacion actual
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if (app == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckOutDialogController::ApplyDialogFields()");
			break;
		}
		
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		//
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs inCheckOutDialogController::ApplyDialogFields()");
			break;
		}
		
		if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, Seccion)==kFalse)
		{
			break;
		}
		
			
		PMString Usuario=this->Aplicar_Usuario_Actual();
			
		/**ID Usuario**/
		PMString UsuarioActual=GetTextControlData(kComboBoxUsuarioWidgetID);;
		PMString Bus="SELECT Id_Empleado From usuario Where Id_Usuario='";
		Bus.Append(UsuarioActual);
		Bus.Append("'");
		PMString IDUsuario=this->BuscarEnUsuarios(Bus,"Id_Empleado");
			
		////////////
		/**Estatus**/
		PMString Estatus=GetTextControlData(kComboBoxStatusWidgetID);

		/**Fecha y hora Out**/
			
		if(IDUsuario.NumUTF16TextChars ()<1)
		{
			IDUsuario = wrkSpcPrefs->GetIDUserLogedString();
		}
		

		PMString Busqueda="SELECT Ruta_Elemento, Nombre_Archivo, Id_Seccion, Id_Estatus, Id_Publicacion, DATE_FORMAT(Fecha_Edicion,'%m/%d/%Y') AS SoloDia, Dirigido_a  FROM pagina WHERE ID=";
		Busqueda.Append(Id_PaginaSel);
		Busqueda.Append(" And Id_Seccion=");
		Busqueda.Append(Seccion);
		//	And Aplicacion='");
		//	Busqueda.Append(Aplicacion);
		//	Busqueda.Append("' And UltimoCheck='1'");
			
		PMString Path;
			
			
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
				
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
			
		K2Vector<PMString> QueryVector;
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
			
		if(QueryVector.size()>0)
		{
			
			Estatus=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[QueryVector.size()-1]);
				
			Path=SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.size()-1]);
				
			Date=SQLInterface->ReturnItemContentbyNameColumn("SoloDia",QueryVector[QueryVector.size()-1]);
				
			Issue=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[QueryVector.size()-1]);
				
			DirigidoA=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[QueryVector.size()-1]);
				
			Seccion=SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[QueryVector.size()-1]);
				
			//Id_PaginaSel=SQLInterface->ReturnItemContentbyNameColumn("Id_PaginaSel",QueryVector[QueryVector.size()-1]);
				
		}
			
		Path.StripWhiteSpace(PMString::kTrailingWhiteSpace);
        
        
#if defined(MACINTOSH)
            if(!Path.Contains(PrefConections.PathOfServerFile)){
                SDKUtilities::convertToMacPath(Path);
                Path = PrefConections.PathOfServerFile+":" +Path;
            }
            else{
                if(Path.IndexOfString("apaginas")>0)
                {
                    if(PMString( Path.GetItem("apaginas", 2)->GetUTF8String().c_str() )!=nil){
                        PMString RutaUniversal = "a";
                        RutaUniversal.Append( PMString( Path.GetItem("apaginas", 2)->GetUTF8String().c_str() ));
                        Path = RutaUniversal;
                    }
                
                    SDKUtilities::convertToMacPath(Path);
                    Path = PrefConections.PathOfServerFile +":" + Path;
                }
                else if(Path.IndexOfString("apaginas")==0)
                    {
                        SDKUtilities::convertToMacPath(Path);
                        Path = PrefConections.PathOfServerFile +":"+ Path;
                    }
            }
        
#elif defined(WINDOWS)
        if(!Path.Contains(PrefConections.PathOfServerFile)){
            SDKUtilities::convertToWinPath(Path);
            Path = PrefConections.PathOfServerFile +"\\" + Path;
        }
        else{
            if(Path.IndexOfString("apaginas")>0)
            {
                if(PMString( Path.GetItem("apaginas", 2)->GetUTF8String().c_str() )!=nil){
                    PMString RutaUniversal = "a";
                    RutaUniversal.Append( PMString( Path.GetItem("apaginas", 2)->GetUTF8String().c_str() ));
                    Path = RutaUniversal;
                }
                
                SDKUtilities::convertToWinPath(Path);
                Path = PrefConections.PathOfServerFile +"\\" + Path;
            }
            else if(Path.IndexOfString("apaginas")==0)
            {
                SDKUtilities::convertToWinPath(Path);
                Path = PrefConections.PathOfServerFile +"\\" + Path;
            }
        }
#endif

		IDFile targetFile;
		FileUtils::PMStringToIDFile(Path,targetFile);
        
        
        
			
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
			
			
		QueryVector.clear();
		Busqueda="CALL PaginaCheckOutStoreProc3("+Id_PaginaSel+",'"+Usuario+"',";
		if(Aplicacion.Contains("InDesign"))
			Busqueda.Append("8");
		else
			Busqueda.Append("14");
			Busqueda.Append(",@A);");

		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
		
		PMString Resultado="";
        if(Aplicacion.Contains("InDesign")){
            if(QueryVector.size()>0)
            {
                
                PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
                if(Privilegio.NumUTF16TextChars()>0 && Privilegio=="1")//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
                {
                    //entonces no tiene privilegios para ver las paginas de esta seccion
                    CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
                    
                }
                else
                {// si no entonces es una regirtro con datos de una pagina
                    for(int32 i=0;i<QueryVector.size();i++)
                    {
                        Resultado=SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
                        if(Resultado!="0")
                        {
                            //CAlert::InformationAlert("Resultado diferente a Cero");
                            break;
                        }
                        
                    }
                }
            }	
            else
            {
                //CAlert::InformationAlert("Ocurrio algun Error");
                break;
            }
            
            if(Resultado!="0")
            {
                CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
                break;
            }
        }
		
			
		/*------------MSSQL----------------------**/
		//K2Vector<PMString> PARAMVector;
			
		//PARAMVector.Append(Issue);
		//PARAMVector.Append(Date);
		//PARAMVector.Append(Seccion);
		//PARAMVector.Append(Id_PaginaSel);
		//PARAMVector.Append(Usuario);	
		//PARAMVector.Append(fecha);	//Fecha_Ult_Mod 
				
		//if(Aplicacion.Contains("InDesign"))
		//	PARAMVector.Append("6");	//ID_Evento
		//else
		//	PARAMVector.Append("14");	//ID_Event
		//PARAMVector.Append("Check Out InDesign");	//Descripcion
			
		//if(checkIn->StoreProcCheckOutPage(PARAMVector)==kFalse)
		// break;
		/*------------MSSQL----------------------**/
			
		UIDRef newDocRef(nil, kInvalidUID);
		UIDRef storyRef(nil, kInvalidUID);
		if(this->CreateAndProcessOpenDocCmd(targetFile, newDocRef))
		{
			//Obtierne Preferencias de CheckOut
			PMString StatusNotaPageOut = GetTextControlData(kComboBoxStatusWidgetID);
			PMString SeccionNotaPageOut = GetTextControlData(kComboBoxSeccionWidgetID);
			PMString PaginaNotaPageOut = GetTextControlData(kPaginaWidgetID);
			PMString IssueNotaPageOut = GetTextControlData(kComboBoxIssueWidgetID);
			PMString DateNotaPageOut = GetTextControlData(kDatePubliWidgetID);
			
			InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																				(
																				 kN2PSQLUtilsBoss,	// Object boss/class
																				 IN2PSQLUtils::kDefaultIID
																				 )));

            int32 indexSelected = getIndexSelectedFromComboWidgetID(kComboBoxUsuarioWidgetID);
			PMString DirigidoANotaPageOut = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario", userandGroups[indexSelected]);//GetTextControlData(kComboBoxUsuarioWidgetID);
			PMString tipoDirigidoA =  SQLInterface->ReturnItemContentbyNameColumn("tipodirigidoa", userandGroups[indexSelected]);
			
			
            ClassDialogCheckOutPaginaSets PreferencesPara;
			checkIn->ObtenerDatosDeCheckOutDPaginas(PreferencesPara);
			
			PreferencesPara.Status_PageOut=		StatusNotaPageOut;
			PreferencesPara.Seccion_PageOut=	SeccionNotaPageOut;
			PreferencesPara.Pagina_PageOut=		PaginaNotaPageOut;
			PreferencesPara.Issue_PageOut=		IssueNotaPageOut;
			PreferencesPara.Date_PageOut=		DateNotaPageOut;
			PreferencesPara.DirididoA_PageOut=	DirigidoANotaPageOut;
			PreferencesPara.tipoDirigidoA_PageOut=		tipoDirigidoA;

            checkIn->GuardaDatosDeCheckOutDPaginas(PreferencesPara);
			/////////////////////////////////////////////
			//Recieve Update
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
				
			InterfacePtr<IDocument> document(newDocRef, IID_IDOCUMENT);	
			//IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			
            //Cambia el estado de edicion de la nota(lapiz)
			if(Aplicacion.Contains("InCopy"))
			{	
				//quiere decir que en InCopy no se encontraba ninguna noda en Edicion
				//por lo tanto si se encontraba en edicion notas en InDesign apareceran con apareceran lapiz de edicion las notas.
				//por lo tanto hay que cambiar el estado de todos lo frames de las notas en InCopy
				//Pone todos los elementos en estado de no edicion
				PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
				if(CadenaCompleta.NumUTF16TextChars()>0)
				{
					UIDRef ur(GetUIDRef(document));
		
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
						ASSERT_FAIL("db is invalid");
						break;
					}
		
		
					int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
					ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
					N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
					for(int32 numElem=0;numElem<contUpdate;numElem++)
					{
						
						PMString IDFrameEleNota="";
						IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						//
							
						UID UIDStory=UID(ListUpdate[numElem].IDFrame);
						UIDRef UIDRefStory=UIDRef(db,UIDStory);
						
						if(numElem==0)	
							storyRef= UIDRefStory;
							
						InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
						if(icStoryList==nil)
							break;
		
		
						icStoryList->ProcessAddStory(UIDRefStory.GetUID());
						
//						ErrorCode error= UpdateStorys->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
//						if (error != kSuccess)
//						{
//							continue;
//						}
					}
//					IDocumentPresentation* docPres = Utils<IDocumentUIUtils>()->GetFrontmostPresentationForDocument(newDocRef.GetDataBase());
//					if (docPres){
//						docPres->MakeActive();
//						CAlert::InformationAlert("MAkeActive");
//					}
//					
//					IActiveContext* ac = GetExecutionContextSession()->GetActiveContext();
//					IControlView* view = ac->GetContextView();
//					if (view)
//					{
//						InterfacePtr<ITextModel> textModel(storyRef, UseDefaultIID());
//						if (docPres && textModel)	// can only select if we have a window and a text story
//						{
//							if (Utils<IGalleyUtils>()->InLayout(docPres)){
//								Utils<IGalleyUtils>()->SetLayoutTextSelection(0, 0, storyRef, kFalse); 
//								CAlert::InformationAlert("SetLayoutTextSelection");
//							}
//							else{
//								Utils<IGalleyUtils>()->SetGalleyTextSelection(0, 0, storyRef, view, kFalse);
//								CAlert::InformationAlert("SetGalleyTextSelection");
//							}
//						}
//					}
//					
//					InterfacePtr<IInCopyStoryList> icStoryList(document, UseDefaultIID());
//					
//					for (int32 i = 0; i < icStoryList->GetStoryCount(); i++)
//					{
//						InterfacePtr<ITextModel> model(icStoryList->GetNthStoryRef(i), UseDefaultIID());
//						if (model)
//						{
//							CAlert::InformationAlert("model OK");
//							InterfacePtr<IFrameList> frameList(model->QueryFrameList());
//							int32 preFrameCount = frameList->GetFrameCount();
//							
//							// braces are for scoping the autosequence and are important, see comment below
//							{
//								IDataBase* db = newDocRef.GetDataBase();
//								CmdUtils::AutoUndoSequencePtr seq(db);
//								IDataBase::SaveRestoreModifiedState smersh(db);
//								
//								InterfacePtr<IInCopyCoreOverflowHandler> overflowHandler(document, UseDefaultIID());
//								overflowHandler->AppendOversetContainer(icStoryList->GetNthStoryRef(i));
//								
//								CAlert::InformationAlert("AppendOversetContainer");
//							}
//							
//							// This labeling of overset frames used to be handled in the AppendOversetContainer call but now with CS4 the column frames are added
//							// on a notification when the sequence point goes out of scope. So the frame list isn't correct until this point
//							for (int32 frameIndex = preFrameCount; frameIndex < frameList->GetFrameCount(); frameIndex++)
//							{
//								InterfacePtr<ITextFrameColumn> newTFC(frameList->QueryNthFrame(frameIndex));
//								InterfacePtr<IContainerClassification> classifier(newTFC, UseDefaultIID());
//								classifier->SetClassification(IContainerClassification::kOversetContainer);
//								CAlert::InformationAlert("SetClassification");
//							}
//						}
//					}
				}
				//Guarda la ruta de donde se abrio la pagina esto con el motivo de verificar la actualizacion 
				//de Documento si es que no fue renombrado por el diseñador
				InterlasaUtilities::SaveXMPVar("N2PPaginaPath",Path,document);
				//CAlert::InformationAlert("Se supone guardo el path"+Path);
				
//				InterfacePtr<IInCopyViewPrefs>	sessionViewPrefs((IInCopyViewPrefs*)::QuerySessionPreferences(IID_IINCOPYVIEWPREFS));
//				IInCopyViewPrefs::ActiveView savedSessionView = sessionViewPrefs->GetActiveView();
//				
//				InterfacePtr<ICommand>	changePrefsCmd( CmdUtils::CreateCommand(kChangeInCopyViewPrefsCmdBoss) );
//				InterfacePtr<IWorkspace>	ws(GetExecutionContextSession()->QueryWorkspace());
//				changePrefsCmd->SetItemList( ::GetUIDRef(ws) );
//				InterfacePtr<IInCopyViewPrefs>	cmdPrefs( changePrefsCmd, UseDefaultIID() );
//				cmdPrefs->SetActiveView( IInCopyViewPrefs::kStory );
//				CmdUtils::ProcessCommand( changePrefsCmd );
			}
            
            
            UpdateStorys->actualizaTodosLosArticulos(document, PrefConections);//UpdateStorys->ActualizaNotasDesdeBD(document, PrefConections);
            N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse, PrefConections);
            UpdateStorys->ShowFrameEdgesDocument( document, kTrue);
        }
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields fin");
	//SystemBeep();
}
//  Generated by Dolly build 17: template "Dialog".
// End, CheckOutDialogController.cpp.

/**
	Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
	para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
	y poner el nombre subre su TextEdit
*/
PMString CheckOutDialogController::Aplicar_Usuario_Actual()
{
	PMString retval="";

	InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
	if (wrkSpcPrefs != nil)
	{	
		retval=wrkSpcPrefs->GetIDUserLogedString();
		retval.SetTranslatable(kFalse);
		SetTextControlData(kComboBoxUsuarioWidgetID,retval);
	}
	return(retval);
}








PMString CheckOutDialogController::BuscarEnUsuarios(PMString &Busqueda,PMString Campo)
{

	PMString cadena="";

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
				
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda=InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.size();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
		}
	}while(false);
		
	
	return(cadena);
}


void CheckOutDialogController::Limpiar_Lista_Paginas(PMString Usuario)
{
	do
	{
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("parent");
			break;
		}

		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
		
			ASSERT_FAIL("panel");
			break;
		}

		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
		
		//se declara crea una lista 
		//SDKListBoxHelper listHelper(ListCView, kN2PCheckInOutPluginID,kPaginasListBoxWidgetID);
		//listHelper.EmptyCurrentListBox();
	/*******************************/
	}while(false);
}


bool16 CheckOutDialogController::CreateAndProcessOpenDocCmd(const IDFile& theFile, UIDRef& newdocUIRef)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString Aplicacion=app->GetApplicationName();
				
		Aplicacion.SetTranslatable(kFalse);
				
		
			
		if(Aplicacion.Contains("InDesign"))
		{
            SDKFileHelper fileHelper(theFile);
            ASSERT(fileHelper.IsExisting());
            if (!fileHelper.IsExisting()) {
                break;
            }
            IDFile templateSysFile = fileHelper.GetIDFile();
            SDKLayoutHelper layoutHelper;
            newdocUIRef = layoutHelper.OpenDocument(templateSysFile);
            
            ASSERT(newdocUIRef != UIDRef::gNull);
            if (newdocUIRef == UIDRef::gNull)
            {
                CAlert::InformationAlert("A02");
                break;
            }
            
            ErrorCode errorCode = layoutHelper.OpenLayoutWindow(newdocUIRef);
            if(errorCode == kSuccess)
            {
                retval=kTrue;
            }


           /* CAlert::InformationAlert("Hey 01");
            SDKLayoutHelper helperLayout;
            
            PMString PackageFolder=	FileUtils::SysFileToPMString (theFile);
            CAlert::InformationAlert("Hey 02: "+PackageFolder);
            newdocUIRef= helperLayout.OpenDocument(theFile, kFullUI);
            CAlert::InformationAlert("Hey 03: "+PackageFolder);
            ErrorCode errorCode = helperLayout.OpenLayoutWindow(newdocUIRef);
            if(errorCode == kSuccess)
            {
                retval=kTrue;
            }
*/
//			// Create an OpenFileWithWindowCmd:
//			InterfacePtr<ICommand>	openFileCmd(CmdUtils::CreateCommand(kOpenFileWithWindowCmdBoss));
//			if(openFileCmd==nil)
//				break;
//			// Get an IOpenFileCmdData Interface for the OpenFileWithWindowCmd:
//			InterfacePtr<IOpenFileCmdData> openFileData(openFileCmd, IID_IOPENFILECMDDATA);
//			if(openFileData==nil)
//				break;
//			// Set the IOpenFileCmdData Interface's data:
//			openFileData->Set(theFile);
//			// Process the OpenFileWithWindowCmd:
//			if (CmdUtils::ProcessCommand(openFileCmd) != kSuccess)
//			{
//				retval=kFalse;
//				break;
//			}
			retval=kTrue;
		}
		else
		{
//			DoOpen(const IDFile& filesToOpen, IOpenFileCmdData::OpenFlags flags, bool16 showWindow = kTrue, IDocument::UndoSupport undoSupport = IDocument::kFullUndoSupport, UIFlags = kFullUI) = 0;
			
					
			newdocUIRef= Utils<IInCopyDocUtils>()->DoOpen(theFile,IOpenFileCmdData::kOpenCopy,kTrue, IDocument::kFullUndoSupport, kMinimalUI);
			
			
			
			
			
			
			/*if (savedSessionView != docView)
			{
				SetView(sessionViewPrefs, docView);
			}
			
			Utils<IInCopyDocUtils>()->SetView(sessionViewPrefs, IInCopyViewPrefs::kLayout);
			 */
			retval=kTrue;
			/*InterfacePtr<ICommand> iOpenDocCmd(CmdUtils::CreateCommand(kTheInCopyOpenDocCmdBoss));
			if(iOpenDocCmd==nil)
			{
				break;
			}
			
			InterfacePtr<IOpenFileCmdData> iOpenFileCmdData(iOpenDocCmd, IID_IOPENFILECMDDATA);
			if(iOpenFileCmdData==nil){
				break;
			}
			
			iOpenFileCmdData->Set(theFile, kSuppressUI);
			CmdUtils::ProcessCommand(iOpenDocCmd);
			
			*/
			/*//openFileData->Set(theFile,kMinimalUI,IOpenFileCmdData::kOpenCopy,IOpenFileCmdData::kUseLockFile);
			InterfacePtr<IK2ServiceRegistry>		registry(GetExecutionContextSession(), IID_IK2SERVICEREGISTRY);
			InterfacePtr<IK2ServiceProvider>	openMgrService(registry->QueryDefaultServiceProvider(kOpenManagerService));
			InterfacePtr<IOpenManager>		openMgr(openMgrService, IID_IOPENMANAGER);
			InterfacePtr<ITriStateData> openMgrOptions(openMgr, IID_ITRISTATEDATA);		
			
			IOpenFileCmdData::OpenFlags flags = openMgrOptions->IsUnknown() ? IOpenFileCmdData::kOpenDefault : openMgrOptions->IsSelected() ?
												IOpenFileCmdData::kOpenOriginal : IOpenFileCmdData::kOpenCopy;

				ErrorCode err = kSuccess;
				
				
					//const IDFile *ithFile = filesToOpen.GetNthFile(i);

					Utils<IInCopyDocUtils>()->DoOpen(theFile, flags);

					err = ErrorUtils::PMGetGlobalErrorCode();
					
					retval=kTrue;
			*/	
		}
		
		
	}while(false);
	return(retval);
}





void CheckOutDialogController::Llenar_ComboUsuarios()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxUsuarioWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);
		
		
		///borrado de la lista al inicializar el combo
		
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		
		//Obtener Grupos de Base de datos
		K2Vector<PMString> groupsN2P;
		PMString Busqueda="SELECT g.Id_Grupo  as Id_Usuario, g.Nombre_Grupo, (SELECT id FROM tipodirigidoa WHERE nombre='Grupo') as tipodirigidoa  From grupo g  WHERE g.estatus<90 ORDER By Nombre_Grupo ASC";
		
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,groupsN2P);
		
		//Consulta Obtiene Usuarios del sistema
		K2Vector<PMString> userN2P;
		Busqueda="SELECT  Id_Usuario as Nombre_Grupo, Id_Usuario , (SELECT id FROM tipodirigidoa WHERE nombre='Usuario') as tipodirigidoa From usuario where estatus<90 ORDER By Id_Usuario ASC";
		
		Busqueda = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,userN2P);
		
		dropListData->AddString("- Grupos", IStringListControlData::kEnd, kTrue, kFalse);
		dropListData->Disable(0);
		userandGroups.clear();
		this->userandGroups.push_back("- Grupos");
		for(int32 i=0;i<groupsN2P.Length();i++)
		{
			userandGroups.push_back(groupsN2P[i]);
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Grupo",groupsN2P[i]);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		dropListData->AddString("-", IStringListControlData::kEnd, kTrue, kFalse);
		userandGroups.push_back("-");
		dropListData->AddString("- Usuarios", IStringListControlData::kEnd, kTrue, kFalse);
		userandGroups.push_back("- Usuarios");
		dropListData->Disable(userandGroups.size()-1);
		for(int32 i=0;i<userN2P.Length();i++)
		{
			userandGroups.push_back(userN2P[i]);
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",userN2P[i]);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		
		dropListData->AddString(" ", IStringListControlData::kEnd, kFalse, kFalse);
		userandGroups.push_back("Nombre_Grupo:©Id_Usuario:©tipodirigidoa:1©");
		
		int32 Index=0;
		for(int32 w=0; w < userandGroups.size(); w++){
			if(PreferencesPara.DirididoA_PageOut == SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",userandGroups[w]) &&
			   PreferencesPara.tipoDirigidoA_PageOut == SQLInterface->ReturnItemContentbyNameColumn("tipodirigidoa",userandGroups[w]))
				Index = w;
		}
		
		
		/*PMString Dirigidoa="";
		 
		 InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		 if (wrkSpcPrefs != nil)
		 {	
		 DirigidoA.Append(wrkSpcPrefs->GetDirigidoANota());
		 }
		 
		 int32 Index=dropListData->GetIndex(Dirigidoa);
		 */
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
		
		////////////////////////////
		
	/*	InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
																				(
																				 kN2PXMPUtilitiesBoss,	// Object boss/class
																				 IN2PXMPUtilities::kDefaultIID
																				 )));
		
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
		
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			K2Vector<PMString> QueryVector;
			Busqueda="SELECT Id_Usuario From bitacora_pagina WHERE Pagina_ID ="+IDPaginaXMP+" AND Id_Evento IN(5,6,13,31,32) ORDER BY ID DESC LIMIT 1";
			
			if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
				CAlert::InformationAlert("Error Lectura de PReferencias E0012");
			
			Busqueda = InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
			SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
			
			PMString cadena="";
			
			for(int32 i=0;i<QueryVector.size();i++)
			{
				cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);		
			}
			
			if(cadena.NumUTF16TextChars()>0)
				this->SetTextControlData(kCheckInUltimoUsuarioQModificoWidgetID,cadena);
			
		}*/
	}while(false);
}


void CheckOutDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("IDDLDrComboBoxSelecPrefer");
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListData->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}


bool16 CheckOutDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From seccion WHERE estatus<90 AND Id_Publicacion=(SELECT Id_Publicacion FROM publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' AND estatus<90) ORDER BY Nombre_de_Seccion ASC";
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda=InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.size();
		
		for(int32 i=0;i<QueryVector.size();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada

		//CAlert::InformationAlert(Id_SeccionText);
		int32 Index=dropListData->GetIndex(Id_SeccionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
			
	}while(false);
	return(kTrue);
}


bool16 CheckOutDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText, PMString iduserloged)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT p.Id_Publicacion, p.Nombre_Publicacion From publicacion p LEFT JOIN grupo g ON g.id_publicacion=p.Id_Publicacion LEFT JOIN usuarioxgrupo uxg ON uxg.Id_Grupo=g.Id_Grupo where uxg.Id_Usuario='"+iduserloged+"' AND (p.estatus is null OR p.estatus<90) AND (g.estatus is null OR g.estatus<90) AND (uxg.estatus is null OR uxg.estatus<90)";
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda=InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.size();
		
		for(int32 i=0;i<QueryVector.size();i++)
		{
			
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//CAlert::InformationAlert(Id_PublicacionText);
		int32 Index=dropListData->GetIndex(Id_PublicacionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}

		
			
	}while(false);
	return(kTrue);
}


bool16 CheckOutDialogController::ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion)
{
	bool16 retval=kFalse;
	
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
		if(myParent==nil)
		{
            CAlert::InformationAlert("myParent==nil");
			break;
		}

        InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
             CAlert::InformationAlert("panel==nil");
			break;
		}

        InterfacePtr<IControlView>		ListCView( panel->FindWidget(kPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
            CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
        
        InterfacePtr<ITreeViewController> 	controller(ListCView, UseDefaultIID());
        ASSERT(controller);
        if(!controller)
        {
            CAlert::InformationAlert("controller");
            break;
        }
        

        InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
        if(SQLInterface==nil)
        {
            CAlert::InformationAlert("SQLInterface==nil");
            break;
        }
        
       

        NodeIDList selectedItems;
        controller->GetSelectedItems(selectedItems);
        const int kSelectionLength =  selectedItems.size() ;
        if (kSelectionLength> 0 )
        {

            PMString dbgInfoString("Selected item(s): ");
        	K2Vector<NodeID>::const_iterator iter, startIter, endIter;
        	startIter = selectedItems.begin();
        	endIter = selectedItems.end();
        	for(iter = startIter; iter != endIter; ++iter)
        	{

                const PageCmpNodeID* oneNode = static_cast<const PageCmpNodeID*>(iter->Get());
        		PMString item = oneNode->GetName();
                
                Id_PaginaSel = SQLInterface->ReturnItemContentbyNameColumn("ID",item);
                Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",item);
                retval=kTrue;
        	}
        }
        else{
             CAlert::InformationAlert("kSelectionLength <= 0");
        }
	}while(false);
	
	return(retval);
}


bool16 CheckOutDialogController::LLenar_Combo_EstatusNota(PMString Id_EstatusActual)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Estatus,Nombre_Estatus From estatus_elemento WHERE Id_tipo_ele='2'  AND estatus<90  ORDER BY Nombre_Estatus ASC";
		if(PrefConections.URLWebServices.NumUTF16TextChars()<=0)
			CAlert::InformationAlert("Error Lectura de PReferencias E0012");
		
		Busqueda=InterlasaUtilities::normalizeWordForPunctuationANDUTF8(Busqueda);
		SQLInterface->SQLQueryDataBase_WebServices(PrefConections.URLWebServices,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.size();i++)
		{
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			PMString IdEstatusQuery=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
			
	}while(false);
	return(kTrue);
}


int32 CheckOutDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}


int32 CheckOutDialogController::getIndexSelectedFromComboWidgetID(WidgetID comboBoxWidgetID)
{
	int32 retval=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(comboBoxWidgetID);//kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		
		
		///borrado de la lista al inicializar el combo
		
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		retval = IDDLDrComboBoxSelecPrefer->GetSelected();
		
	}while(false);
	return retval;
}
