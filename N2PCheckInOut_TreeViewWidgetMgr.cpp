//========================================================================================
//  
//  $File: //depot/devtech/nevis/plugin/source/sdksamples/wlistboxcomposite/WLBCmpTreeViewWidgetMgr.cpp $
//  
//  Owner: Danielle Darling
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2017/03/10 00:54:55 $
//  
//  $Revision: #8 $
//  
//  $Change: 979292 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "IControlView.h"
#include "IInLineEditController.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"

#include "CTreeViewWidgetMgr.h"
#include "N2PCheckInOut_NodeID.h"
#include "N2PCheckInOutID.h"
#include "WLBCmpRezDefs.h"
#include "IN2PSQLUtils.h"
#include "CAlert.h"


/**
 *  Tree view manager
	@ingroup wlistboxcomposite
 */
class WLBCmpTVWidgetMgr : public CTreeViewWidgetMgr
{
public:
	WLBCmpTVWidgetMgr(IPMUnknown *boss);
	virtual ~WLBCmpTVWidgetMgr()
	{}
    
    virtual bool16	ApplyDataToWidget( const NodeID& node, IPanelControlData* widgetList, int32 message ) const;

    void RegisterStyles()
    {
        RegisterStyleWidget(kLargePaletteRowsTreeStyle, kWLBCmpListElementRsrcID, kN2PCheckInOutPluginID);
    }
};


CREATE_PMINTERFACE(WLBCmpTVWidgetMgr, kWLBCmpTVWidgetMgrImpl)

WLBCmpTVWidgetMgr::WLBCmpTVWidgetMgr(IPMUnknown *boss) :
CTreeViewWidgetMgr(boss, kList)
{
}



bool16 WLBCmpTVWidgetMgr::ApplyDataToWidget(const NodeID& node, 
                                            IPanelControlData* widgetList, 
                                            int32 message) const
{
	do
	{
		TreeNodePtr<PageCmpNodeID> nodeID(node);
		PMString listName = nodeID->GetName();
        
        InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
        if(SQLInterface==nil)
        {
            ASSERT_FAIL("SQLInterface = nil");
            break;
        }

        PMString Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",listName);
        PMString Secc=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",listName);
        PMString Estado=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",listName);
        PMString Folio_Pagina=SQLInterface->ReturnItemContentbyNameColumn("ID",listName);
        PMString Id_SeccionText =SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",listName);
        PMString Id_PublicacionText=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",listName);

        
	      
		IControlView* nameView = widgetList->FindWidget(kPaginaLabelWidgetID);
	        
		InterfacePtr<ITextControlData>	textControlData( nameView, UseDefaultIID() );
		ASSERT(textControlData);
		if( textControlData== nil) {
			break;
		}
		textControlData->SetString(Nombre_Archivo);
        
        
        IControlView* SeccionView = widgetList->FindWidget(kSeccionLabelWidgetID);
        InterfacePtr<ITextControlData>	textSeccionControlData( SeccionView, UseDefaultIID() );
        ASSERT(textSeccionControlData);
        if( textSeccionControlData== nil) {
            break;
        }
        textSeccionControlData->SetString(Secc);
        
        IControlView* FolioView = widgetList->FindWidget(kFolio_PaginaLabelWidgetID);
        InterfacePtr<ITextControlData>	textFolioControlData( FolioView, UseDefaultIID() );
        ASSERT(textFolioControlData);
        if( textFolioControlData== nil) {
            break;
        }
        textFolioControlData->SetString(Folio_Pagina);
        
        IControlView* EstadoView = widgetList->FindWidget(kEstadoLabelWidgetID);
        InterfacePtr<ITextControlData>	textEstadoControlData( EstadoView, UseDefaultIID() );
        ASSERT(textEstadoControlData);
        if( textEstadoControlData== nil) {
            break;
        }
        textEstadoControlData->SetString(Estado);
        
        IControlView* PublicacionView = widgetList->FindWidget(kLabelId_PublicacionLabelWidgetID);
        InterfacePtr<ITextControlData>	textPublicacionControlData( PublicacionView, UseDefaultIID() );
        ASSERT(textPublicacionControlData);
        if( textPublicacionControlData== nil) {
            break;
        }
        textPublicacionControlData->SetString(Id_PublicacionText);
        
        IControlView* Id_SeccionView = widgetList->FindWidget(kLabelId_SeccionLabelWidgetID);
        InterfacePtr<ITextControlData>	textId_SeccionViewControlData( Id_SeccionView, UseDefaultIID() );
        ASSERT(textId_SeccionViewControlData);
        if( textId_SeccionViewControlData== nil) {
            break;
        }
        textId_SeccionViewControlData->SetString(Id_SeccionText);
        
        
        IControlView* Id_EstatusView = widgetList->FindWidget(kLabelId_EstatusNotaLabelWidgetID);
        InterfacePtr<ITextControlData>	Id_EstatusViewControlData( Id_EstatusView, UseDefaultIID() );
        ASSERT(Id_EstatusViewControlData);
        if( Id_EstatusViewControlData== nil) {
            break;
        }
        Id_EstatusViewControlData->SetString(Estado);
	} while (false);

	return kTrue;
}

