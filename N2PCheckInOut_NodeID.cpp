//========================================================================================
//  
//  $File: //depot/devtech/nevis/plugin/source/sdksamples/wlistboxcomposite/WLBCmpNodeID.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2017/03/10 00:54:55 $
//  
//  $Revision: #8 $
//  
//  $Change: 979292 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPMStream.h"

// Project includes:
#include "N2PCheckInOut_NodeID.h"
/**
	@ingroup wlistboxcomposite
*/

/* Compare
*/
int32 PageCmpNodeID::Compare(const NodeIDClass* nodeID) const
{
	const PageCmpNodeID* oneNode = static_cast<const PageCmpNodeID*>(nodeID);
	ASSERT(nodeID);
    return fName.Compare(kTrue, oneNode->GetName());

}


/* Clone
*/
NodeIDClass* PageCmpNodeID::Clone() const
{
	return new PageCmpNodeID(this->GetName());
}


/* Read
*/
void PageCmpNodeID::Read(IPMStream* stream)
{
	fName.ReadWrite(stream);
}


/* Write
*/
void PageCmpNodeID::Write(IPMStream* stream) const
{
	(const_cast<PageCmpNodeID*>(this)->fName).ReadWrite(stream);
}

//	end, File:	PnlTrvDataNode.cpp
